
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

  Nest is [MIT licensed](LICENSE).
## Document
Identify Service

Includes functions role to Admin and User: 
- User

   + Get User: http://localhost:3000/api/users
   + Get User by email: http://localhost:3000/api/users/{email}
   + Login with email and password: http://localhost:3000/api/auth/login
   + Send verify token when you forgot password and reset password: http://localhost:3000/api/auth/email/forgot-password/{email}
   + Reset Password when you forgot password: PIN token sent in your register email.Don’t fill in CurrentPassword fields.
     http://localhost:3000/api/auth/email/reset-password
   + Reset new Password: PIN token sent in your register email.
     http://localhost:3000/api/auth/email/reset-password

- Admin
   + Get User: http://localhost:3000/api/users
   + Get User by email: http://localhost:3000/api/users/{email}
   + Register new User : http://localhost:3000/api/users/register
   + Update User : http://localhost:3000/api/users/update 
   + Delete User by Id: http://localhost:3000/api/users/{id}
