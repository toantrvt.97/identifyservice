import { Module } from '@nestjs/common';
import { EmailToken } from '../../emailtoken.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { AuthController } from 'src/auth/auth.controller';

@Module({
    imports: [TypeOrmModule.forFeature([EmailToken])],
  })
export class EmailtokenModule {}
