import { Module } from '@nestjs/common';
import { ForgotPassword } from '../../forgotpassword.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from 'src/auth/auth.service';
import { AuthController } from 'src/auth/auth.controller';
@Module({
    imports: [TypeOrmModule.forFeature([ForgotPassword])],
  })
export class ForgotpasswordModule {}
