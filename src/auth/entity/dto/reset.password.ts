import { ApiProperty } from '@nestjs/swagger';

export class ResetPassword {
    @ApiProperty()
    readonly email: string;

    @ApiProperty()
    readonly newpassword: string;

    @ApiProperty()
    readonly newpasswordtoken: number;

    @ApiProperty()
    readonly currentpassword: string;
}
