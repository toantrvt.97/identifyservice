import * as jwt from 'jsonwebtoken';
import { default as config } from '../config';
import { Injectable } from '@nestjs/common';
import { UserEntity } from '../users/entity/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class JWTService {
  constructor(@InjectRepository(UserEntity)
  private readonly userrepository: Repository<UserEntity>) { }

  async createToken(email: string, roles: string[]) {
    // tslint:disable-next-line:one-variable-per-declaration
    const expiresIn = config.jwt.expiresIn,
      secretOrKey = config.jwt.secretOrKey;
    const userInfo = { email, roles };
    const token = jwt.sign(userInfo, secretOrKey, { expiresIn });
    return {
      // expires_in: expiresIn,
      access_token: token,
    };
  }

  async validateUser(signedUser): Promise<UserEntity> {
    const userFromDb = await this.userrepository.findOne({ email: signedUser.email });
    if (userFromDb) {
      return userFromDb;
    }
    return null;
  }
}
