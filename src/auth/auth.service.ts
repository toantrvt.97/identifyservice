import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JWTService } from './jwt.service';
import * as bcrypt from 'bcryptjs';
import { Roles } from 'src/common/decorators/roles.decorator';
import { InjectRepository } from '@nestjs/typeorm';
import { EmailToken } from './entity/emailtoken.entity';
import { Repository, InsertResult, Transaction, DeleteResult } from 'typeorm';
import { UserEntity } from 'src/users/entity/user.entity';
import { ForgotPassword } from './entity/forgotpassword.entity';
import * as nodemailer from 'nodemailer';
import { async } from 'rxjs/internal/scheduler/async';
import {default as config} from '../config';

@Injectable()
export class AuthService {
    jwt: any;
    constructor(
        @InjectRepository(EmailToken)
        private readonly emailtokenRepo: Repository<EmailToken>,
        @InjectRepository(ForgotPassword)
        private readonly forgotPasswordRepo: Repository<ForgotPassword>,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private readonly usersService: UsersService,
        private readonly jwtService: JWTService) { }

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.usersService.findbyemail(email);
        if (user && user.password === password) {
            // tslint:disable-next-line:no-shadowed-variable
            const { password, ...result } = user;
            return result;
        }
        return null;
    }
    async validateLogin(email: string, password: string) {
        const userdb = await this.usersService.findbyemail(email);
        if (!userdb) { throw new HttpException('LOGIN.USER_NOT_FOUND', HttpStatus.NOT_FOUND); }
        const isValidPass = await bcrypt.compare(password, userdb.password);
        if (isValidPass) {
            const accesstoken = this.jwtService.createToken(email, userdb.roles);
            return accesstoken;
        } else {

            throw new HttpException('Login Error', HttpStatus.UNAUTHORIZED);
        }
    }
    async createEmailToken(email: string): Promise<boolean> {
        const emailVerifi = await this.emailtokenRepo.findOne(email);
        if (emailVerifi && ((new Date().getTime() - emailVerifi.timestamp.getTime()) / 60000 < 5)) {
            throw new HttpException('Login email sended', HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            const emailVe = new EmailToken();
            emailVe.email = email;
            emailVe.emailtoken = Math.floor(Math.random() * (9000000)) + 1000000; // Generate 7 digits number
            emailVe.timestamp = new Date();
        }
        return true;
    }
    async createForgotPasswordToken(email: string): Promise<ForgotPassword> {
        const fotgotPassword = await this.forgotPasswordRepo.findOne({email});
        if (fotgotPassword && ((new Date().getTime() - fotgotPassword.timestamp.getTime()) / 60000 < 5)) {
            throw new HttpException('Reset password email sended recently', HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            const forgot = new ForgotPassword();
            forgot.email = email;
            forgot.newpasswordtoken = Math.floor(Math.random() * (9000000)) + 1000000; // Generate 7 digits number
            forgot.timestamp = new Date();
            await this.forgotPasswordRepo.save(forgot);
            return (forgot);
        }
    }
    async getForgotPassword(newpasswordtoken: number): Promise<ForgotPassword> {
        return await this.forgotPasswordRepo.findOne({newpasswordtoken});
    }

    async checkPassword(email: string, password: string) {
        const userDb = await this.usersService.findbyemail(email);
        if (!userDb) { throw new HttpException('Login user not found', HttpStatus.NOT_FOUND); }
        return await bcrypt.compare(password, userDb.password);
    }
    async sendEmailForgotPassword(email: string): Promise<boolean> {
        const userDb = await this.userRepository.findOne({email});
        if (!userDb) { throw new HttpException('Login user not found', HttpStatus.NOT_FOUND); }
        const tokenModel = await this.createForgotPasswordToken(email);
        if (tokenModel && tokenModel.newpasswordtoken) {
            const transporter = nodemailer.createTransport({
                host: config.mail.host,
                port: config.mail.port,
                secure: true,
                auth: {
                    user: config.mail.user,
                    pass: config.mail.pass,
                },
            });
            const mailOptions = {
                from: '3S sky team',
                to: email,
                subject: 'Forgotten password',
                test: 'Forgot password',
                html: 'Hi! <br><br> If you requested to reset your password.<br><br>' +
                    '<a href=' + config.host.url + ':' + config.host.port + '/api/auth/email/reset-password/'
                    + tokenModel.newpasswordtoken + '>Click here </a>',

            };
            const sent = await new Promise<boolean>(async function (resolve, reject) {
                return await transporter.sendMail(mailOptions, async (Error, info) => {
                    if (Error) {
                        console.log('Message sent: %s', Error);
                        return reject(false);
                    }
                    console.log('Message sent: %s', info.messageId);
                    resolve(true);
                });
            });
            return sent;
        } else {
            throw new HttpException('User not register', HttpStatus.FORBIDDEN);
        }
    }
    // delete forgotpassword when was changed
    async deleteforgotpassword( email: string): Promise<DeleteResult> {
        return await this.forgotPasswordRepo.delete({email});
    }
}
