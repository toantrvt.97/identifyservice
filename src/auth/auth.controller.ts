import { Controller, Request, Post, UseGuards, Get, HttpCode, HttpStatus, Body, Param } from '@nestjs/common';
import { Login } from './interfaces/login.interface';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { ApiOperation, ApiResponse, ApiTags, ApiBearerAuth, ApiProperty, ApiBody } from '@nestjs/swagger';
import { ResetPassword } from './entity/dto/reset.password';
import { AuthGuard } from '@nestjs/passport';
@Controller('api/auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) { }
  @Post('login')
  @ApiOperation({ summary: 'Login with email and password' })
  public async login(@Body() login: Login) {
    try {
      const respone = await this.authService.validateLogin(login.email, login.password);
      return respone;

    } catch (e) {
      return e;
    }
  }
  @Get('email/forgot-password/:email')
  @ApiOperation({ summary: 'Send mail to create PIN' })
  public async sendEmailForgotPassword(@Param() params) {
    try {
      const isEmailSent = await this.authService.sendEmailForgotPassword(params.email);
    } catch (e) {
      return e;
    }
  }
  @Post('email/forgot-password')
  @ApiOperation({ summary: 'Fill in email,newpassword,and newpasswordtoken; not fill in currentpassword ' })
  @HttpCode(HttpStatus.OK)
  public async setNewPassword(@Body() resetPassword: ResetPassword) {
    try {
      let isNewPasswordChanged: boolean = false;
      if (resetPassword.newpasswordtoken) {
        const forgottenPassword = await this.authService.getForgotPassword(resetPassword.newpasswordtoken);
        isNewPasswordChanged = await this.userService.setPassword(forgottenPassword.email, resetPassword.newpassword);
        if (isNewPasswordChanged) { await this.authService.deleteforgotpassword(resetPassword.email); }
      }
      return isNewPasswordChanged;
    } catch (e) {
      return e;
    }
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('email/reset-password')
  @ApiOperation({ summary: 'Fill in all fields ' })
  @HttpCode(HttpStatus.OK)
  public async resetNewPassword(@Body() resetPassword: ResetPassword) {
      try {
          let isNewPasswordChanged: boolean = false;
          if (resetPassword.email && resetPassword.currentpassword && resetPassword.newpasswordtoken) {
              const isValidPassword = await this.authService.checkPassword(resetPassword.email, resetPassword.currentpassword);
              if (isValidPassword) {
                  isNewPasswordChanged = await this.userService.setPassword(resetPassword.email, resetPassword.newpassword);
              }
          }
          return isNewPasswordChanged;
      } catch (e) {
          return e;
      }
  }
}
