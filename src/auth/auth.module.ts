import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { AuthController } from './auth.controller';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './passport/jwt.strategy';
import { JWTService } from './jwt.service';
import { UserEntity } from 'src/users/entity/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailToken } from './entity/emailtoken.entity';
import { ForgotPassword } from './entity/forgotpassword.entity';
import { ForgotpasswordModule } from './entity/module/forgotpassword/forgotpassword.module';
import { EmailtokenModule } from './entity/module/emailtoken/emailtoken.module';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    ForgotpasswordModule,
    EmailtokenModule,
    TypeOrmModule.forFeature([UserEntity]),
    TypeOrmModule.forFeature([EmailToken]),
    TypeOrmModule.forFeature([ForgotPassword]),
    ],
  providers: [AuthService, LocalStrategy, JWTService, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule { }
