import { ApiProperty } from '@nestjs/swagger';

export class Login {

    @ApiProperty()
    readonly email: string;
    @ApiProperty()
    readonly password: string;
  }
