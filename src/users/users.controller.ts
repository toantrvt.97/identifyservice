import { Controller, Get, Param, Post, Body, Put, Delete, UseGuards, HttpCode, HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserEntity } from './entity/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/role.guard';
import { Roles } from 'src/common/decorators/roles.decorator';
import { ApiBearerAuth, ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { ResetPassword } from 'src/auth/entity/dto/reset.password';
import { AuthService } from 'src/auth/auth.service';
@Controller('api/users')
@ApiTags('users')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
    ) { }
    @Get()
    @UseGuards(RolesGuard)
    findAll(): Promise<UserEntity[]> {
        return this.usersService.findAll();
    }
    @Get(':email')
    @UseGuards(RolesGuard)
    async get(@Param() params) {
        try {
            const user = await this.usersService.findbyemail(params.email);
            return user;
        } catch (error) {
            return error;
        }
    }
    @Post('register')
    @ApiOperation({ summary: 'Admin register new User' })
    @UseGuards(RolesGuard)
    @Roles('Admin')
    create(@Body() user: UserEntity) {
        return this.usersService.create(user);
    }
    @Put('update')
    @ApiOperation({ summary: 'Admin update User' })
    @UseGuards(RolesGuard)
    @Roles('Admin')
    update(@Body() user: UserEntity) {
        return this.usersService.update(user);
    }
    @Delete(':id')
    @ApiOperation({ summary: 'Admin delete User' })
    @UseGuards(RolesGuard)
    @Roles('Admin')
    delete(@Param() params) {
        return this.usersService.delete(params.id);
    }

}
