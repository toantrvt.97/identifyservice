import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Length, IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column({ default: null })
    @Length(4, 50)
    @ApiProperty()
    name: string;

    @IsEmail(undefined, { message: 'Username is not a valid email address.' })
    @Column({ default: null })
    @Length(3, 50)
    @ApiProperty()
    email: string;

    @Column({ default: null })
    @ApiProperty()
    password: string;

    @Column('simple-array')
    @ApiProperty({ type: [String] })
    @IsNotEmpty()
    roles: string[];

    @Column({ default: null })
    @CreateDateColumn()
    @ApiProperty()
    createdAt: Date;

    @Column({ default: null })
    @UpdateDateColumn()
    @ApiProperty()
    updateAt: Date;
}
