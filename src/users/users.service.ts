import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { UserEntity } from './entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';

const saltRounds = 10;

@Injectable()
export class UsersService {
    constructor(@InjectRepository(UserEntity)
    private readonly userrepository: Repository<UserEntity>) { }
    async findAll(): Promise<UserEntity[]> {
        return await this.userrepository.find();
    }
    async findbyemail(email: string): Promise<UserEntity | undefined> {
        return this.userrepository.findOne({ email });
    }
    async create(user: UserEntity): Promise<UserEntity> {
        return await this.userrepository.save(user);
    }
    async update(user: UserEntity): Promise<UpdateResult> {
        return await this.userrepository.update(user.id, user);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.userrepository.delete(id);
    }

    isValidEmail(email: string) {
        if (email) {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        } else {
            return false;
        }
    }

    async createNewUser(user: UserEntity): Promise<UserEntity> {
        if (this.isValidEmail(user.email) && user.password) {
            const userregisterd = await this.findbyemail(user.email);
            if (!userregisterd) {
                user.password = await bcrypt.hash(user.password, saltRounds);
                const createUser = this.userrepository.create(user);
            //   createUser.roles = ['User'];
                return await this.userrepository.save(createUser);
            } else {
                throw new HttpException('REGISTRATION.USER_ALREADY_REGISTERED', HttpStatus.FORBIDDEN);
            }
        } else {
            throw new HttpException('REGISTRATION.MISSING_MANDATORY_PARAMETERS', HttpStatus.FORBIDDEN);
        }
    }

    async setPassword(email: string, newpassword: string): Promise<boolean> {
        const userDb = await this.userrepository.findOne({ email });
        if (!userDb) {
            throw new HttpException('LOGIN.USER_NOT_FOUND', HttpStatus.NOT_FOUND);
        }

        userDb.password = await bcrypt.hash(newpassword, saltRounds);

        await this.userrepository.save(userDb);
        return true;
    }
}
