import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailtokenModule } from './auth/entity/module/emailtoken/emailtoken.module';
import { ForgotpasswordModule } from './auth/entity/module/forgotpassword/forgotpassword.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'sa',
    password: 'VietToan97',
    database: 'identifydata',
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: true,
  }),
  UsersModule, EmailtokenModule, ForgotpasswordModule, AuthModule],
  controllers: [AppController],
  providers: [ AppService],

})
export class AppModule { }
