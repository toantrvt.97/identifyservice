declare const _default: {
    jwt: {
        secretOrKey: string;
        expiresIn: number;
    };
    host: {
        url: string;
        port: string;
    };
    mail: {
        host: string;
        port: number;
        secure: boolean;
        user: string;
        pass: string;
    };
};
export default _default;
