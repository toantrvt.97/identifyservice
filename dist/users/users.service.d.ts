import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { UserEntity } from './entity/user.entity';
export declare class UsersService {
    private readonly userrepository;
    constructor(userrepository: Repository<UserEntity>);
    findAll(): Promise<UserEntity[]>;
    findbyemail(email: string): Promise<UserEntity | undefined>;
    create(user: UserEntity): Promise<UserEntity>;
    update(user: UserEntity): Promise<UpdateResult>;
    delete(id: any): Promise<DeleteResult>;
    isValidEmail(email: string): boolean;
    createNewUser(user: UserEntity): Promise<UserEntity>;
    setPassword(email: string, newpassword: string): Promise<boolean>;
}
