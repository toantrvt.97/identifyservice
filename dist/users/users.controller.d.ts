import { UsersService } from './users.service';
import { UserEntity } from './entity/user.entity';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    findAll(): Promise<UserEntity[]>;
    get(params: any): Promise<any>;
    create(user: UserEntity): Promise<UserEntity>;
    update(user: UserEntity): Promise<import("typeorm").UpdateResult>;
    delete(params: any): Promise<import("typeorm").DeleteResult>;
}
