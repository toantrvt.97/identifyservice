"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
let UserEntity = class UserEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiProperty(),
    __metadata("design:type", Number)
], UserEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    class_validator_1.Length(4, 50),
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UserEntity.prototype, "name", void 0);
__decorate([
    class_validator_1.IsEmail(undefined, { message: 'Username is not a valid email address.' }),
    typeorm_1.Column({ default: null }),
    class_validator_1.Length(3, 50),
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UserEntity.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], UserEntity.prototype, "password", void 0);
__decorate([
    typeorm_1.Column('simple-array'),
    swagger_1.ApiProperty({ type: [String] }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Array)
], UserEntity.prototype, "roles", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    typeorm_1.CreateDateColumn(),
    swagger_1.ApiProperty(),
    __metadata("design:type", Date)
], UserEntity.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    typeorm_1.UpdateDateColumn(),
    swagger_1.ApiProperty(),
    __metadata("design:type", Date)
], UserEntity.prototype, "updateAt", void 0);
UserEntity = __decorate([
    typeorm_1.Entity()
], UserEntity);
exports.UserEntity = UserEntity;
//# sourceMappingURL=user.entity.js.map