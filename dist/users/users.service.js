"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./entity/user.entity");
const typeorm_2 = require("@nestjs/typeorm");
const bcrypt = require("bcryptjs");
const saltRounds = 10;
let UsersService = class UsersService {
    constructor(userrepository) {
        this.userrepository = userrepository;
    }
    async findAll() {
        return await this.userrepository.find();
    }
    async findbyemail(email) {
        return this.userrepository.findOne({ email });
    }
    async create(user) {
        return await this.userrepository.save(user);
    }
    async update(user) {
        return await this.userrepository.update(user.id, user);
    }
    async delete(id) {
        return await this.userrepository.delete(id);
    }
    isValidEmail(email) {
        if (email) {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        else {
            return false;
        }
    }
    async createNewUser(user) {
        if (this.isValidEmail(user.email) && user.password) {
            const userregisterd = await this.findbyemail(user.email);
            if (!userregisterd) {
                user.password = await bcrypt.hash(user.password, saltRounds);
                const createUser = this.userrepository.create(user);
                return await this.userrepository.save(createUser);
            }
            else {
                throw new common_1.HttpException('REGISTRATION.USER_ALREADY_REGISTERED', common_1.HttpStatus.FORBIDDEN);
            }
        }
        else {
            throw new common_1.HttpException('REGISTRATION.MISSING_MANDATORY_PARAMETERS', common_1.HttpStatus.FORBIDDEN);
        }
    }
    async setPassword(email, newpassword) {
        const userDb = await this.userrepository.findOne({ email });
        if (!userDb) {
            throw new common_1.HttpException('LOGIN.USER_NOT_FOUND', common_1.HttpStatus.NOT_FOUND);
        }
        userDb.password = await bcrypt.hash(newpassword, saltRounds);
        await this.userrepository.save(userDb);
        return true;
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map