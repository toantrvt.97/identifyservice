"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const users_module_1 = require("../users/users.module");
const passport_1 = require("@nestjs/passport");
const local_strategy_1 = require("./local.strategy");
const auth_controller_1 = require("./auth.controller");
const jwt_strategy_1 = require("./passport/jwt.strategy");
const jwt_service_1 = require("./jwt.service");
const user_entity_1 = require("../users/entity/user.entity");
const typeorm_1 = require("@nestjs/typeorm");
const emailtoken_entity_1 = require("./entity/emailtoken.entity");
const forgotpassword_entity_1 = require("./entity/forgotpassword.entity");
const forgotpassword_module_1 = require("./entity/module/forgotpassword/forgotpassword.module");
const emailtoken_module_1 = require("./entity/module/emailtoken/emailtoken.module");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            users_module_1.UsersModule,
            passport_1.PassportModule,
            forgotpassword_module_1.ForgotpasswordModule,
            emailtoken_module_1.EmailtokenModule,
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.UserEntity]),
            typeorm_1.TypeOrmModule.forFeature([emailtoken_entity_1.EmailToken]),
            typeorm_1.TypeOrmModule.forFeature([forgotpassword_entity_1.ForgotPassword]),
        ],
        providers: [auth_service_1.AuthService, local_strategy_1.LocalStrategy, jwt_service_1.JWTService, jwt_strategy_1.JwtStrategy],
        controllers: [auth_controller_1.AuthController],
        exports: [auth_service_1.AuthService],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map