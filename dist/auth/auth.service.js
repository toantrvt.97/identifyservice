"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const jwt_service_1 = require("./jwt.service");
const bcrypt = require("bcryptjs");
const roles_decorator_1 = require("../common/decorators/roles.decorator");
const typeorm_1 = require("@nestjs/typeorm");
const emailtoken_entity_1 = require("./entity/emailtoken.entity");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../users/entity/user.entity");
const forgotpassword_entity_1 = require("./entity/forgotpassword.entity");
const nodemailer = require("nodemailer");
const config_1 = require("../config");
let AuthService = class AuthService {
    constructor(emailtokenRepo, forgotPasswordRepo, userRepository, usersService, jwtService) {
        this.emailtokenRepo = emailtokenRepo;
        this.forgotPasswordRepo = forgotPasswordRepo;
        this.userRepository = userRepository;
        this.usersService = usersService;
        this.jwtService = jwtService;
    }
    async validateUser(email, password) {
        const user = await this.usersService.findbyemail(email);
        if (user && user.password === password) {
            const { password } = user, result = __rest(user, ["password"]);
            return result;
        }
        return null;
    }
    async validateLogin(email, password) {
        const userdb = await this.usersService.findbyemail(email);
        if (!userdb) {
            throw new common_1.HttpException('LOGIN.USER_NOT_FOUND', common_1.HttpStatus.NOT_FOUND);
        }
        const isValidPass = await bcrypt.compare(password, userdb.password);
        if (isValidPass) {
            const accesstoken = this.jwtService.createToken(email, userdb.roles);
            return accesstoken;
        }
        else {
            throw new common_1.HttpException('Login Error', common_1.HttpStatus.UNAUTHORIZED);
        }
    }
    async createEmailToken(email) {
        const emailVerifi = await this.emailtokenRepo.findOne(email);
        if (emailVerifi && ((new Date().getTime() - emailVerifi.timestamp.getTime()) / 60000 < 5)) {
            throw new common_1.HttpException('Login email sended', common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            const emailVe = new emailtoken_entity_1.EmailToken();
            emailVe.email = email;
            emailVe.emailtoken = Math.floor(Math.random() * (9000000)) + 1000000;
            emailVe.timestamp = new Date();
        }
        return true;
    }
    async createForgotPasswordToken(email) {
        const fotgotPassword = await this.forgotPasswordRepo.findOne({ email });
        if (fotgotPassword && ((new Date().getTime() - fotgotPassword.timestamp.getTime()) / 60000 < 5)) {
            throw new common_1.HttpException('Reset password email sended recently', common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            const forgot = new forgotpassword_entity_1.ForgotPassword();
            forgot.email = email;
            forgot.newpasswordtoken = Math.floor(Math.random() * (9000000)) + 1000000;
            forgot.timestamp = new Date();
            await this.forgotPasswordRepo.save(forgot);
            return (forgot);
        }
    }
    async getForgotPassword(newpasswordtoken) {
        return await this.forgotPasswordRepo.findOne({ newpasswordtoken });
    }
    async checkPassword(email, password) {
        const userDb = await this.usersService.findbyemail(email);
        if (!userDb) {
            throw new common_1.HttpException('Login user not found', common_1.HttpStatus.NOT_FOUND);
        }
        return await bcrypt.compare(password, userDb.password);
    }
    async sendEmailForgotPassword(email) {
        const userDb = await this.userRepository.findOne({ email });
        if (!userDb) {
            throw new common_1.HttpException('Login user not found', common_1.HttpStatus.NOT_FOUND);
        }
        const tokenModel = await this.createForgotPasswordToken(email);
        if (tokenModel && tokenModel.newpasswordtoken) {
            const transporter = nodemailer.createTransport({
                host: config_1.default.mail.host,
                port: config_1.default.mail.port,
                secure: true,
                auth: {
                    user: config_1.default.mail.user,
                    pass: config_1.default.mail.pass,
                },
            });
            const mailOptions = {
                from: '3S sky team',
                to: email,
                subject: 'Forgotten password',
                test: 'Forgot password',
                html: 'Hi! <br><br> If you requested to reset your password.<br><br>' +
                    '<a href=' + config_1.default.host.url + ':' + config_1.default.host.port + '/api/auth/email/reset-password/'
                    + tokenModel.newpasswordtoken + '>Click here </a>',
            };
            const sent = await new Promise(async function (resolve, reject) {
                return await transporter.sendMail(mailOptions, async (Error, info) => {
                    if (Error) {
                        console.log('Message sent: %s', Error);
                        return reject(false);
                    }
                    console.log('Message sent: %s', info.messageId);
                    resolve(true);
                });
            });
            return sent;
        }
        else {
            throw new common_1.HttpException('User not register', common_1.HttpStatus.FORBIDDEN);
        }
    }
    async deleteforgotpassword(email) {
        return await this.forgotPasswordRepo.delete({ email });
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(emailtoken_entity_1.EmailToken)),
    __param(1, typeorm_1.InjectRepository(forgotpassword_entity_1.ForgotPassword)),
    __param(2, typeorm_1.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        users_service_1.UsersService,
        jwt_service_1.JWTService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map