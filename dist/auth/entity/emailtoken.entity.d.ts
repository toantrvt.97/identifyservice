export declare class EmailToken {
    id: number;
    email: string;
    emailtoken: number;
    timestamp: Date;
}
