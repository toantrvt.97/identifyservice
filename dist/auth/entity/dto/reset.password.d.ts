export declare class ResetPassword {
    readonly email: string;
    readonly newpassword: string;
    readonly newpasswordtoken: number;
    readonly currentpassword: string;
}
