export declare class ForgotPassword {
    id: number;
    email: string;
    newpasswordtoken: number;
    timestamp: Date;
}
