"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const login_interface_1 = require("./interfaces/login.interface");
const auth_service_1 = require("./auth.service");
const users_service_1 = require("../users/users.service");
const swagger_1 = require("@nestjs/swagger");
const reset_password_1 = require("./entity/dto/reset.password");
const passport_1 = require("@nestjs/passport");
let AuthController = class AuthController {
    constructor(authService, userService) {
        this.authService = authService;
        this.userService = userService;
    }
    async login(login) {
        try {
            const respone = await this.authService.validateLogin(login.email, login.password);
            return respone;
        }
        catch (e) {
            return e;
        }
    }
    async sendEmailForgotPassword(params) {
        try {
            const isEmailSent = await this.authService.sendEmailForgotPassword(params.email);
        }
        catch (e) {
            return e;
        }
    }
    async setNewPassword(resetPassword) {
        try {
            let isNewPasswordChanged = false;
            if (resetPassword.newpasswordtoken) {
                const forgottenPassword = await this.authService.getForgotPassword(resetPassword.newpasswordtoken);
                isNewPasswordChanged = await this.userService.setPassword(forgottenPassword.email, resetPassword.newpassword);
                if (isNewPasswordChanged) {
                    await this.authService.deleteforgotpassword(resetPassword.email);
                }
            }
            return isNewPasswordChanged;
        }
        catch (e) {
            return e;
        }
    }
    async resetNewPassword(resetPassword) {
        try {
            let isNewPasswordChanged = false;
            if (resetPassword.email && resetPassword.currentpassword && resetPassword.newpasswordtoken) {
                const isValidPassword = await this.authService.checkPassword(resetPassword.email, resetPassword.currentpassword);
                if (isValidPassword) {
                    isNewPasswordChanged = await this.userService.setPassword(resetPassword.email, resetPassword.newpassword);
                }
            }
            return isNewPasswordChanged;
        }
        catch (e) {
            return e;
        }
    }
};
__decorate([
    common_1.Post('login'),
    swagger_1.ApiOperation({ summary: 'Login with email and password' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_interface_1.Login]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    common_1.Get('email/forgot-password/:email'),
    swagger_1.ApiOperation({ summary: 'Send mail to create PIN' }),
    __param(0, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "sendEmailForgotPassword", null);
__decorate([
    common_1.Post('email/forgot-password'),
    swagger_1.ApiOperation({ summary: 'Fill in email,newpassword,and newpasswordtoken; not fill in currentpassword ' }),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [reset_password_1.ResetPassword]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "setNewPassword", null);
__decorate([
    swagger_1.ApiBearerAuth(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.Post('email/reset-password'),
    swagger_1.ApiOperation({ summary: 'Fill in all fields ' }),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [reset_password_1.ResetPassword]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "resetNewPassword", null);
AuthController = __decorate([
    common_1.Controller('api/auth'),
    swagger_1.ApiTags('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        users_service_1.UsersService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map