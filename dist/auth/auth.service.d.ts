import { UsersService } from '../users/users.service';
import { JWTService } from './jwt.service';
import { EmailToken } from './entity/emailtoken.entity';
import { Repository, DeleteResult } from 'typeorm';
import { UserEntity } from 'src/users/entity/user.entity';
import { ForgotPassword } from './entity/forgotpassword.entity';
export declare class AuthService {
    private readonly emailtokenRepo;
    private readonly forgotPasswordRepo;
    private readonly userRepository;
    private readonly usersService;
    private readonly jwtService;
    jwt: any;
    constructor(emailtokenRepo: Repository<EmailToken>, forgotPasswordRepo: Repository<ForgotPassword>, userRepository: Repository<UserEntity>, usersService: UsersService, jwtService: JWTService);
    validateUser(email: string, password: string): Promise<any>;
    validateLogin(email: string, password: string): Promise<{
        access_token: string;
    }>;
    createEmailToken(email: string): Promise<boolean>;
    createForgotPasswordToken(email: string): Promise<ForgotPassword>;
    getForgotPassword(newpasswordtoken: number): Promise<ForgotPassword>;
    checkPassword(email: string, password: string): Promise<any>;
    sendEmailForgotPassword(email: string): Promise<boolean>;
    deleteforgotpassword(email: string): Promise<DeleteResult>;
}
