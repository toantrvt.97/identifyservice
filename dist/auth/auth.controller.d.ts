import { Login } from './interfaces/login.interface';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { ResetPassword } from './entity/dto/reset.password';
export declare class AuthController {
    private readonly authService;
    private readonly userService;
    constructor(authService: AuthService, userService: UsersService);
    login(login: Login): Promise<any>;
    sendEmailForgotPassword(params: any): Promise<any>;
    setNewPassword(resetPassword: ResetPassword): Promise<any>;
    resetNewPassword(resetPassword: ResetPassword): Promise<any>;
}
