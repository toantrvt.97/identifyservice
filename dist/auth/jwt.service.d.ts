import { UserEntity } from '../users/entity/user.entity';
import { Repository } from 'typeorm';
export declare class JWTService {
    private readonly userrepository;
    constructor(userrepository: Repository<UserEntity>);
    createToken(email: string, roles: string[]): Promise<{
        access_token: string;
    }>;
    validateUser(signedUser: any): Promise<UserEntity>;
}
