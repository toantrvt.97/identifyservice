"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const config_1 = require("../config");
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../users/entity/user.entity");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
let JWTService = class JWTService {
    constructor(userrepository) {
        this.userrepository = userrepository;
    }
    async createToken(email, roles) {
        const expiresIn = config_1.default.jwt.expiresIn, secretOrKey = config_1.default.jwt.secretOrKey;
        const userInfo = { email, roles };
        const token = jwt.sign(userInfo, secretOrKey, { expiresIn });
        return {
            access_token: token,
        };
    }
    async validateUser(signedUser) {
        const userFromDb = await this.userrepository.findOne({ email: signedUser.email });
        if (userFromDb) {
            return userFromDb;
        }
        return null;
    }
};
JWTService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], JWTService);
exports.JWTService = JWTService;
//# sourceMappingURL=jwt.service.js.map